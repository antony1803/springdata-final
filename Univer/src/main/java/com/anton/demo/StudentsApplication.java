package com.nikita.demo;


import com.nikita.demo.repositories.SubjectRepository;
import com.nikita.demo.repositories.MarkBookRepository;
import com.nikita.demo.repositories.UniversityGroupRepository;
import com.nikita.demo.repositories.StudentRepository;
import com.nikita.demo.univer.Subject;
import com.nikita.demo.univer.Mark_book;
import com.nikita.demo.univer.UniversityGroup;
import com.nikita.demo.univer.Student;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.naming.Name;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class StudentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentsApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(StudentRepository studentRepository,MarkBookRepository markBookRepository, UniversityGroupRepository universityGroupRepository,
                                   SubjectRepository subjectRepository){
        return (args) -> {
            universityGroupRepository.save(new UniversityGroup(5));
            universityGroupRepository.save(new UniversityGroup(1));

            subjectRepository.save(new Subject("Math"));
            subjectRepository.save(new Subject("PE"));
            subjectRepository.save(new Subject("English"));

            markBookRepository.save(new Mark_book(100000));
            markBookRepository.save(new Mark_book(8));
            markBookRepository.save(new Mark_book(4));

            List<Subject> disciplineEntities = new ArrayList<>();
            disciplineEntities.add(subjectRepository.findByName("Math"));


            List<Student> studentsi = new ArrayList<>();



            disciplineEntities.add(subjectRepository.findByName("PE"));
            studentRepository.save(new Student("Anton", "Kukyan", universityGroupRepository.findByGroup(1),
                    markBookRepository.findByGradebook(8), disciplineEntities));

            disciplineEntities.add(subjectRepository.findByName("PE"));
            studentRepository.save(new Student("Nikita", "Titov", universityGroupRepository.findByGroup(1),
                    markBookRepository.findByGradebook(4), disciplineEntities));

            studentsi.add(studentRepository.findByNameAndSurname("Nikita","Titov"));

            if  (studentsi.size() !=0){
                subjectRepository.save(new Subject("bio",studentsi));
            }



        };
    }
}
