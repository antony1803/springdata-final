package com.nikita.demo.repositories;


import com.nikita.demo.univer.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer> {
     Student findByNameAndSurname(String name, String surname);
}
